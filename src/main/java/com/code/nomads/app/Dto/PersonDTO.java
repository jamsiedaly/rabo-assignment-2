package com.code.nomads.app.Dto;

import java.time.Instant;

public class PersonDTO {

    private final String firstName;
    private final String lastName;
    private final Instant dateOfBirth;

    public PersonDTO(String firstName, String lastName, Instant dateOfBirth, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Instant getDateOfBirth() {
        return dateOfBirth;
    }

}
