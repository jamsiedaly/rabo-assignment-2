package com.code.nomads.app.Dto;

import java.time.Instant;

public class PetDTO {

    private final String name;
    private final Instant dateOfBirth;

    public PetDTO(String name, Instant dateOfBirth) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public Instant getDateOfBirth() {
        return dateOfBirth;
    }
}
