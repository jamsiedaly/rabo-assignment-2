package com.code.nomads.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableConfigurationProperties({ApplicationProperties.class})
public class App {

    public static void main (String[] args) {
        ApplicationContext ctx = SpringApplication.run(App.class, args);
    }
}
