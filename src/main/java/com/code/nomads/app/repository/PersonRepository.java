package com.code.nomads.app.repository;

import com.code.nomads.app.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("SELECT p FROM Person p WHERE p.firstName = ?1 and p.lastName = ?2")
    Optional<Person> getOneByFirstNameAndLastName(String firstName, String lastName);

    @Query("SELECT p FROM Person p " +
            "WHERE UPPER(p.firstName) LIKE UPPER(CONCAT('%',?1,'%')) " +
            "or UPPER(p.lastName) LIKE UPPER(CONCAT('%',?1,'%'))")
    Optional<Person> searchPeople(String searchTerm);

}