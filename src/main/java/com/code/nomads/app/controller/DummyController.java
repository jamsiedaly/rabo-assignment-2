package com.code.nomads.app.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/")
@RestController
public class DummyController {

    @GetMapping
    public String hello() {
        return "Everything seems to be working ok";
    }
}
