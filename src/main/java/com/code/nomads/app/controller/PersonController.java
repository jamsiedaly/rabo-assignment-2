package com.code.nomads.app.controller;

import com.code.nomads.app.Dto.PersonDTO;
import com.code.nomads.app.entities.Person;
import com.code.nomads.app.entities.Pet;
import com.code.nomads.app.exception.DuplicateNameException;
import com.code.nomads.app.repository.PersonRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static com.code.nomads.app.controller.Routes.PERSON;

@RequestMapping(PERSON)
@RestController
public class PersonController {

    private final PersonRepository personRepository;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping
    public List<Person> getAllPeople() {
        return personRepository.findAll();
    }

    @GetMapping("/{id}")
    public Person getPerson(@PathVariable long id) {
        return personRepository.getOne(id);
    }

    @GetMapping("/{query}")
    public Person searchForPersonByName(@PathVariable String query) {
        Optional<Person> result = personRepository.searchPeople(query);
        return result.orElse(null);
    }

    @GetMapping("/name")
    public Person getPersonByName(@RequestBody String firstName, @RequestBody String lastName) {
        Optional<Person> result = personRepository.getOneByFirstNameAndLastName(firstName, lastName);
        return result.orElseThrow(PersonNotFoundException::new);
    }

    @PutMapping("{/id}")
    public Person updatePerson(@PathVariable long id, @RequestBody PersonDTO person) {
        if(isNameTakenByOtherUser(id, person))
            throw new DuplicateNameException();
        Person personEntity = new Person();
        personEntity.setId(id);
        personEntity.setFirstName(person.getFirstName());
        personEntity.setLastName(person.getLastName());
        personEntity.setDateOfBirth(person.getDateOfBirth());
        return personRepository.save(personEntity);
    }

    private boolean isNameTakenByOtherUser(long id, PersonDTO person) {
        Optional<Person> result = personRepository.getOneByFirstNameAndLastName(person.getFirstName(), person.getLastName());
        if (result.isPresent() && result.get().getId() != id)
            return true;
        return false;
    }

    @PostMapping
    public Person createPerson(@RequestBody PersonDTO person) {
        Optional<Person> result = personRepository.getOneByFirstNameAndLastName(person.getFirstName(), person.getLastName());
        checkIfNameIsTaken(result);
        Person personEntity = new Person();
        personEntity.setFirstName(person.getFirstName());
        personEntity.setLastName(person.getLastName());
        personEntity.setDateOfBirth(person.getDateOfBirth());
        return personRepository.save(personEntity);
    }

    @PostMapping("{id}/pet/{petId}")
    public void addPet(@PathVariable long id, @RequestBody Pet pet) {
        Person person = personRepository.getOne(id);
        person.addPet(pet);
    }

    private void checkIfNameIsTaken(Optional<Person> originalPerson) {
        if (originalPerson.isPresent())
            throw new DuplicateNameException();
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Person Not Found")
    private class PersonNotFoundException extends RuntimeException {}

}
