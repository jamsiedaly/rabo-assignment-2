package com.code.nomads.app.controller;

public class Routes {

    public final static String PERSON = "/person";
    public final static String ADDRESS = "/person/{id}/address";
    public final static String PET = "/pet";

}
