package com.code.nomads.app.controller;

import com.code.nomads.app.Dto.PetDTO;
import com.code.nomads.app.entities.Pet;
import com.code.nomads.app.repository.PetRepository;
import org.springframework.web.bind.annotation.*;

import static com.code.nomads.app.controller.Routes.PET;

@RequestMapping(PET)
@RestController
public class PetController {

    private final PetRepository petRepository;

    public PetController(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @GetMapping("/{id}")
    public Pet getPet(@PathVariable long id) {
        return petRepository.getOne(id);
    }

    @PostMapping
    public void createPet(@RequestBody PetDTO pet) {
        Pet petEntity = new Pet(pet.getName(), pet.getDateOfBirth());
        petRepository.save(petEntity);
    }

    @DeleteMapping("/{id}")
    public void deletePet(@PathVariable long id) {
        petRepository.deleteById(id);
    }
}
