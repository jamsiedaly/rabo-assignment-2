package com.code.nomads.app.controller;

import com.code.nomads.app.entities.Person;
import com.code.nomads.app.repository.PersonRepository;
import org.springframework.web.bind.annotation.*;

import static com.code.nomads.app.controller.Routes.ADDRESS;

@RequestMapping(ADDRESS)
@RestController
public class AddressController {

    private final PersonRepository personRepository;

    public AddressController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @PostMapping
    public void updateAddress(@PathVariable long id, @RequestBody String address) {
        Person person = personRepository.getOne(id);
        person.setAddress(address);
        personRepository.save(person);
    }
}
