package com.code.nomads.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "date_of_birth")
    private Instant dateOfBirth;

    @ManyToOne
    @JsonIgnoreProperties("pets")
    private Person person;

    public Pet(String name, Instant dateOfBirth) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public void setPerson(Person person) {
        if (this.person != null) {
            Person previousOwner = this.person;
            this.person = null;
            previousOwner.removePet(this);
        }
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
}
