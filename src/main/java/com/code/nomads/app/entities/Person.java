package com.code.nomads.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "person",
        uniqueConstraints={
                @UniqueConstraint(columnNames = {"first_name", "last_name"})
        })
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "date_of_birth")
    private Instant dateOfBirth;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "person")
    private List<Pet> pets = new ArrayList<>();

    @Override
    public String toString() {
        return "Person{" +
                "id=" + getId() +
                ", firstName='" + getFirstName() + "'" +
                ", lastName='" + getLastName() + "'" +
                ", dateOfBirth='" + getDateOfBirth() + "'" +
                ", address='" + getAddress() + "'" +
                "}";
    }

    public Instant getDateOfBirth() {
        return dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setDateOfBirth(Instant dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person addPet(Pet pet) {
        this.pets.add(pet);
        pet.setPerson(this);
        return this;
    }

    public Person removePet(Pet pet) {
        this.pets.remove(pet);
        pet.setPerson(null);
        return this;
    }

    public List<Pet> getPets() {
        return pets;
    }
}
