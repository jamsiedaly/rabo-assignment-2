package com.code.nomads.app.entities;

import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

class PersonTest {

    @Test
    void addingPetRemovesFromOldOwner() {
        Pet pet = new Pet("SNIFFLES", Instant.now());
        Person firstOwner = new Person();
        firstOwner.setFirstName("BOB");
        firstOwner.addPet(pet);
        Person secondOwner = new Person();
        secondOwner.setFirstName("GERRY");

        assertThat(pet.getPerson().equals(firstOwner));
        assertThat(firstOwner.getPets().contains(pet));

        secondOwner.addPet(pet);

        assertThat(!firstOwner.getPets().contains(pet));
        assertThat(pet.getPerson().equals(secondOwner));
        assertThat(secondOwner.getPets().contains(pet));
    }
}