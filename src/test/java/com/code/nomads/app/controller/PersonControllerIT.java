package com.code.nomads.app.controller;

import com.code.nomads.app.App;
import com.code.nomads.app.Dto.PersonDTO;
import com.code.nomads.app.entities.Person;
import com.code.nomads.app.exception.DuplicateNameException;
import com.code.nomads.app.repository.PersonRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ContextConfiguration(classes=App.class)
public class PersonControllerIT {

    private final PersonController personController;
    private final PersonRepository personRepository;

    private Person person;
    private Person personA;

    PersonControllerIT(
            @Autowired PersonController personController,
            @Autowired PersonRepository personRepository
    ) {
        this.personController = personController;
        this.personRepository = personRepository;
        person = new Person();
        personA = new Person();
    }

    @BeforeEach
    void setUp() {
        person.setFirstName("FIRST_NAME");
        person.setLastName("LAST_NAME");
        person.setAddress("ADDRESS");
        person.setDateOfBirth(Instant.now());
    }

    @AfterEach
    void tearDown() {
        personRepository.deleteAll();
    }

    @Test
    void getAllPeople() throws Exception {
        personRepository.save(person);
        personRepository.save(personA);

        List<Person> result = personController.getAllPeople();
        assertThat(result.size() == 2);
    }

    @Test
    void getPersonTest() {
        Person savedPerson = personRepository.saveAndFlush(person);

        personController.getPerson(savedPerson.getId());

        List<Person> personList = personRepository.findAll();
        Person lastPerson = personList.get(personList.size() - 1);

        assertThat(lastPerson.equals(savedPerson));
        assertThat(lastPerson.getLastName().equals(person.getLastName()));
        assertThat(lastPerson.getAddress().equals(person.getAddress()));
        assertThat(lastPerson.getDateOfBirth().equals(person.getDateOfBirth()));
        assertThat(lastPerson.getId().equals(savedPerson.getId()));
    }

    @Test
    void getPersonByLastName() {
        Person savedPerson = personRepository.saveAndFlush(person);

        Person foundPerson = personController.searchForPersonByName(savedPerson.getLastName());

        assertThat(foundPerson.equals(savedPerson));
        assertThat(foundPerson.getLastName().equals(person.getLastName()));
        assertThat(foundPerson.getAddress().equals(person.getAddress()));
        assertThat(foundPerson.getDateOfBirth().equals(person.getDateOfBirth()));
        assertThat(foundPerson.getId().equals(savedPerson.getId()));
    }

    @Test
    void getPersonByFirstName() {
        Person savedPerson = personRepository.saveAndFlush(person);

        Person foundPerson = personController.searchForPersonByName(savedPerson.getLastName());

        assertThat(foundPerson.equals(savedPerson));
        assertThat(foundPerson.getLastName().equals(person.getLastName()));
        assertThat(foundPerson.getAddress().equals(person.getAddress()));
        assertThat(foundPerson.getDateOfBirth().equals(person.getDateOfBirth()));
        assertThat(foundPerson.getId().equals(savedPerson.getId()));
    }

    @Test
    void getPersonByFullName() {
        Person savedPerson = personRepository.saveAndFlush(person);

        Person foundPerson = personController.getPersonByName(savedPerson.getFirstName(), savedPerson.getLastName());

        assertThat(foundPerson.equals(savedPerson));
        assertThat(foundPerson.getLastName().equals(person.getLastName()));
        assertThat(foundPerson.getAddress().equals(person.getAddress()));
        assertThat(foundPerson.getDateOfBirth().equals(person.getDateOfBirth()));
        assertThat(foundPerson.getId().equals(savedPerson.getId()));
    }

    @Test
    void updatePerson() {
        Person savedPerson = personRepository.saveAndFlush(person);
        String updatedName = "NEW_FIRST_NAME";
        savedPerson.setFirstName(updatedName);
        PersonDTO updatedPerson = personEntityToPersonDto(savedPerson);

        personController.updatePerson(savedPerson.getId(), updatedPerson);

        List<Person> personList = personRepository.findAll();
        Person lastPerson = personList.get(personList.size() - 1);

        assertThat(lastPerson.getFirstName().equals(updatedName));
    }

    @Test
    void createPerson() {
        PersonDTO dto = personEntityToPersonDto(person);

        personController.createPerson(dto);

        List<Person> personList = personRepository.findAll();
        Person lastPerson = personList.get(personList.size() - 1);

        assertThat(lastPerson.getFirstName().equals(person.getFirstName()));
        assertThat(lastPerson.getLastName().equals(person.getLastName()));
        assertThat(lastPerson.getDateOfBirth().equals(person.getDateOfBirth()));
    }

    @Test
    void cannotCreatePersonWithSameName() {
        personRepository.save(person);
        PersonDTO dto = personEntityToPersonDto(person);

        Assertions.assertThrows(DuplicateNameException.class, () -> {
            personController.createPerson(dto);
        });
    }

    public PersonDTO personEntityToPersonDto(Person person) {
        return new PersonDTO(
                person.getFirstName(),
                person.getLastName(),
                person.getDateOfBirth(),
                person.getAddress()
        );
    }
}