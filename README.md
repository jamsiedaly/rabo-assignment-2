# Rabo Assignment

Here is a spring boot based system. It allows users to store people and their pets.

The username and password is:
    
    U: admin
    P: admin
    
To stand up an instance of docker simply run:

     docker-compose -f mysql.yml up -d
     
Tests can be run with:

    mvn verify
    
The service can be stood up using:
    
    mvn spring-boot:run
    
Make sure to send me any questions at james@codenomads.nl